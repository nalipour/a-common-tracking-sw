#! /usr/bin/env python
import requests
import argparse
import re

def test_response(response):
    response.raise_for_status()

def get_project_id(url,project_name,token):
    r = requests.get(url + "projects",headers={"PRIVATE-TOKEN":token})
    test_response(r)
    proj_list = r.json()
    matches = [proj_dict for proj_dict in proj_list if proj_dict["path_with_namespace"] == project_name]
    if len(matches) > 1:
        print "more than one project found matching the given project path"
        print "going to use the first one"
    return matches[0]["id"]

def get_merge_request_labels(args):
    r = requests.get(args.mr_url,headers={"PRIVATE-TOKEN":args.token})
    test_response(r)
    mr_info = r.json()
    return mr_info['labels']

def main():
    parser = argparse.ArgumentParser(description="GitLab merge request label extractor",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--project",required=True,help="GitLab project with namespace (e.g. user/my-project)")
    parser.add_argument("--url",default="https://gitlab.cern.ch",help="URL of GitLab instance")
    parser.add_argument("--merge-request-id",dest="mr_id",required=True,type=int,help="(unique) ID of merge request (not the project specific IID)")
    parser.add_argument("--outfile",default="env.properties",help="output file with environment variables")
    parser.add_argument("--token",required=True,help="private GitLab user token")

    args = parser.parse_args()

    # turn GitLab instance url into API access point
    if args.url[-1] != "/":
        args.url += "/"
    args.url += "api/v3/"

    # get unique project and MR id
    args.project_id = get_project_id(args.url,args.project,args.token)
    args.mr_url = args.url + "projects/{0}/merge_request/{1}".format(args.project_id,args.mr_id)
    
    labels = get_merge_request_labels(args)
    print "# merge request has labels: ",labels
    out = open(args.outfile,'w')
    if "static code checker" in labels:
        print >> out, "RUN_SCA=TRUE"
    else:
        print >> out, "RUN_SCA=FALSE"
    if "skip documentation" in labels:
        print >> out, "BUILD_DOC=FALSE"
    else:
        print >> out, "BUILD_DOC=TRUE"
    out.close()

    known_labels = ["static code checker","skip documentation"]
    not_used = [l for l in labels if l not in known_labels]
    if not_used:
        print "# could not interpret the following labels: ",not_used
    
if __name__ == "__main__":
    main()
